                        Gnash 0.8.8 Released!

We just released an improved GNU Flash player, Gnash 0.8.8.  Gnash
plays SWF (Shockwave Flash) files compatible with the Adobe Flash
player.  Gnash is portable software released under the GNU GPLv3.  It
runs on GNU/Linux, embedded GNU + Linux systems, and BSD, including
x86, ARM, MIPS, PowerPC, and 64-bit systems.  It comes with a
standalone player as well as a browser plugin compatible with
Firefox, Chrome, Konqueror, and all Gecko-based browsers.

Improvements since the 0.8.7 release are:

    * 100% of all YouTube videos should work. If you have problems,
      delete all YouTube cookies and refresh.
    * Gnash can switch at runtime between the Cairo, OpenGL, and AGG
      renderers.
    * Gnash can switch media handlers at runtime, too, between FFmpeg
      and Gstreamer,
    * Gnash can now decode video quickly on hardware compatible with the
      VAAPI library (a few NVidia, ATI, and Intel graphics processors).
    * Gnash now compiles faster due to reduced internal dependencies.
    * Scriptable Plugin support so Javascript in the browser can
      work with ActionScript in Gnash.
    * Improved input device handling when using a raw framebuffer.

See the NEWS file for more improvements.

You can grab the Gnash sources from
http://ftp.gnu.org/pub/gnu/gnash/0.8.8, or from Gnash Git using the
release_0_8_8 branch. Experimental binary packages built by the Gnash
team are also available at
http://www.getgnash.org/packages, along with source snapshots. Binary
packages for Debian or RPM based systems will be available from your
GNU/Linux distribution, and from whatever BSD variant you are using.

Questions about Gnash or offers of help can go to the developer's email
list at address@hidden Free sofware doesn't exists without your
support, for donation information, please please go to
http://www.openmedianow.org/?q=node/32.

Checksums:

aec414ee3bebb8901054818fae735214  gnash-0.8.8.tar.gz
ce57f66e222f7eb1adf7f7b4a1274612  gnash-0.8.8.tar.bz2
2dab24bf90e4312db9cd3e5645d81164  gnash-0.8.8.tar.gz.sig
151a16f614e899458ef9ef758585169b  gnash-0.8.8.tar.bz2.sig

