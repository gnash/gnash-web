The third beta release of Gnash has just been made at version
0.8.4. Gnash is a GPL'd SWF movie player and browser plugin for
Firefox, Mozilla, and Konqueror. Gnash supports many SWF v7 features
and ActionScript 2 classes. with growing support for SWF v8 and
v9. Gnash also runs on many GNU/Linux distributions, embedded
GNU/Linux, FreeBSD, NetBSD, OpenBSD, non x86 processors, and 64 bit
architectures. Ports to Darwin and Windows are in progress for a
future release. The plugin works best with Firefox 1.0.4 or newer, and
should work in any Mozilla based browser using NPAPI. There is also a
standalone player for GNOME or KDE based desktops.

Improvements since 0.8.3 release are:

    * Keep Adobe happy with our users and our users happy with us by
      changing "Flash player" into "SWF player" everywhere.  Adobe
      claims "Flash" as a trademark and had asked a Linux distributor
      to fix it.
    * The popular SWF Twitter badge now renders correctly.
    * Fix parsing of urls containing multiple question marks
    * Fix support for movies embedding multiple sound streams
    * Support for loading PNG and GIF images added.
    * Improved rendering of SWF movies because of the less visible
      changes listed below.
    * Support for writing RGB/RGBA PNG images and JPEG images.
    * Works with Potlatch OpenStreetMap editor
    * New 'flvdumper' utility for analyzing FLV video files.
    * XPI packaging support for Mozilla & Firefox.

See the NEWS file for more improvements.

Gnash supports the majority of Flash opcodes up to SWF version 7, and 
a wide sampling of ActionScript 2 classes for SWF version 8.5. Flash
version 9 and ActionScript 3 support is being worked on.  All the
core ones are implemented, and many of the newer ones work, but may be
missing some of their methods. If the browser only displays a blank
window, it is likely because of an unimplemented feature. All
unimplemented opcodes and ActionScript classes and methods print a
warning when using -v with gnash or gprocessor. Using gprocessor -v is
a quick way to see why a movie isn't playing correctly.

You can grab the Gnash sources from ftp://ftp.gnu.org/pub/gnu/gnash/0.8.4,
or from Gnash Bzr from the branch .../gnas-0.8.4 .  Binary packages for
Debian or RPM based systems will be available from your GNU/Linux
distribution, and from whatever BSD variant you are using. Experimental
binary packages built by the Gnash team are also available at
http://www.getgnash.org, along with source snapshots. Please
report packaging bugs upstream to your distribution. If you think you
have found a bug in Gnash, then you should file as complete a report
as possible at https://savannah.gnu.org/bugs/?group=gnash. Patches are
always preferred to bug reports, as this is a community project. You
can submit patches at https://savannah.gnu.org/patch/?group=gnash, or
email them to gnash-dev@gnu.org.

Please include the operating system name and version, compiler
version, and which gnash version you are using, in your bug
reports. For bugs in the plugin, please also add the browser and
it's version. Gnash does not support Firefox versions below 1.0.4.

Questions about Gnash or offers of help can go to the developers email
list at gnash-dev@gnu.org.

b47a7ed4b7c66b8c2ebacc2286ef0d4b  gnash-0.8.4.tar.bz2
3100e8d669795c03167a171937897c83  gnash-0.8.4.tar.gz

